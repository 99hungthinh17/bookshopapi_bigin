﻿using System;
using System.Collections.Generic;
using BookShopApi.Models;
using BookShopApi.Services;
using FluentValidation;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace BookShopApi.Commands.BookCommands
{
    public class CreateBookCommand : IRequest<Book>
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string CategoryId { get; set; }
        public string Author { get; set; }
        public int Amount { get; set; }
        public int Code { get; set; }
    }
    public class BookValidator : AbstractValidator<CreateBookCommand>
    {
        private readonly BookService _bookService;
        public BookValidator(BookService bookService)
        {
            _bookService = bookService;
            RuleFor(book => book.Name).NotNull();
            RuleFor(book => book.Price).NotNull();
            RuleFor(book => book.CategoryId).NotNull();
            RuleFor(book => book.Author).NotNull();
            RuleFor(book => book.Amount).NotNull();
            RuleFor(book => book.Code).NotNull().MustAsync(async (code, cancellation) => {
                bool exists = await _bookService.GetAsync(code);
                return !exists;
            }).WithMessage("Book is already existed");
        }

        
    }
    public class CreateBookCommandHandler : IRequestHandler<CreateBookCommand, Book>
    {
        private readonly BookService _bookService;
        public CreateBookCommandHandler(BookService bookService)
        {
            _bookService = bookService;
        }

        public async Task<Book> Handle(CreateBookCommand request, CancellationToken cancellationToken)
        {
            var Book = new Book
            {
                Name = request.Name,
                Price = request.Price,
                CategoryId = request.CategoryId,
                Author = request.Author,
                Amount = request.Amount,
                Code = request.Code
            };
            return await _bookService.CreateAsync(Book);
        }
        public async Task<bool> UniqueCode(string code)
        {
            var book = await _bookService.GetAsync(code);
            if (book == null)
                return false;
            return true;
        }

    }
}
