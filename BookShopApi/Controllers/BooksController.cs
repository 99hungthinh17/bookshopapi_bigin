﻿
using BookShopApi.Models;
using BookShopApi.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net.WebSockets;
using System.Threading.Tasks;
using BookShopApi.Controllers;
using Microsoft.AspNetCore.Authorization;
using MediatR;
using BookShopApi.Commands.BookCommands;
using BookShopApi.Queries.BookQueries;

namespace BookShopApi.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class BooksController : ControllerBase
    {
        private IMediator _mediator;
        public BooksController(IMediator mediator)
        {
            _mediator = mediator;
          
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Book>>> GetAll()
        {
            var books = await _mediator.Send(new GetBooksQuery());

            if (books == null)
            {
                return NotFound();
            }

            return Ok(books);
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<Book>> GetbyId(string id)
        {
            var book = await _mediator.Send(new GetBookByIdQuery { Id = id });

            if (book == null)
            {
                return NotFound();
            }

            return Ok(book);
        }
        [HttpGet("[action]")]
        public async Task<ActionResult<Book>> SearchBook(string searchString)
        {
            var books = await _mediator.Send(new SearchBooksQuery {Name=searchString });
            return Ok(books);
        }

        [HttpPost]
       
        public async Task<IActionResult> Create(CreateBookCommand command)
        {
             var createdBook = await _mediator.Send(command);
             return Ok(createdBook);

        }
    }
}