﻿
using BookShopApi.Models;
using BookShopApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookShopApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly CategoryService _categoryService;

        public CategoriesController(CategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        [HttpGet]
     
        public async Task<ActionResult<IEnumerable<Category>>> GetAll()
        {
            var categories = await _categoryService.GetAsync();
            return Ok(categories);
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<Category>> GetbyId(string id)
        {
            var category = await _categoryService.GetAsync(id);
            if (category == null)
            {
                return BadRequest("Category not found");
            }
            return Ok(category);
        }
        [HttpPost]
   
        public async Task<IActionResult> Create(Category category)
        {
            await _categoryService.CreateAsync(category);
            return Ok(category);
        }

        [HttpPut]    
        public async Task<IActionResult> Update(string id, Category updatedCategory)
        {
            var catergoy = await _categoryService.GetAsync(id);
            if (catergoy == null)
            {
                return BadRequest("Category not found");
            }
            await _categoryService.UpdateAsync(id, updatedCategory);
            return Ok("Updated Successfully");
        }

        [HttpDelete] 
        public async Task<IActionResult> Delete(string id)
        {
            await _categoryService.DeleteAsync(id);
            return Ok("Delete sucessfully");
        }
    }
}