﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookShopApi.Models
{
    public class BookstoreDatabaseSettings 
    {
        public string BooksCollectionName { get; set; }
        public string CategoriesCollectionName { get; set; }
        public string UsersCollectionName { get; set; }
        public string ShoppingCartsCollectionName { get; set; }
        public string PurchaseHistoriesCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    } 
}
