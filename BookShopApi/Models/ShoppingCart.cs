﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookShopApi.Models
{
    public class ShoppingCart
    {   
        public string Id { get; set; }     
        public string UserId { get; set; }    
        public List<string> BookIds { get; set; }
        public List<Book> Books { get; set; }

    }
}
