﻿using BookShopApi.Models;
using BookShopApi.Services;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BookShopApi.Queries.BookQueries
{
    public class GetBooksQuery : IRequest<IEnumerable<Book>>
    {
        public class GetAllBooksQueryHandler : IRequestHandler<GetBooksQuery, IEnumerable<Book>>
        {
            private readonly BookService _bookService;

            public GetAllBooksQueryHandler(BookService bookService)
            {
                _bookService = bookService;
            }
            public async Task<IEnumerable<Book>> Handle(GetBooksQuery query, CancellationToken cancellationToken)
            {
                var books = await _bookService.GetAsync();
                if (books == null)
                {
                    return null;
                }
                return books;
            }
        }
    }

    public class GetBookByIdQuery : IRequest<Book>
    {
        public string Id { get; set; }
        public class GetBookByIdQueryHandler : IRequestHandler<GetBookByIdQuery, Book>
        {
            private readonly BookService _bookService;

            public GetBookByIdQueryHandler(BookService bookService)
            {
                _bookService = bookService;
            }
            public async Task<Book> Handle(GetBookByIdQuery query, CancellationToken cancellationToken)
            {
                var book = await _bookService.GetAsync(query.Id);
                if (book == null) return null;
                return book;
            }
        }
    }

    public class SearchBooksQuery : IRequest<IEnumerable<Book>>
    {
        public string Name { get; set; }
        public class SearchBooksQueryHandler : IRequestHandler<SearchBooksQuery, IEnumerable <Book>>
        {
            private readonly BookService _bookService;

            public SearchBooksQueryHandler(BookService bookService)
            {
                _bookService = bookService;
            }
            public async Task<IEnumerable<Book>> Handle(SearchBooksQuery query, CancellationToken cancellationToken)
            {
                var books = await _bookService.SearchAsync(query.Name);
                if (books == null) return null;
                return books;
            }          
        }
    }
}
