﻿
using BookShopApi.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookShopApi.Services
{
    public class CategoryService
    {
        private readonly IMongoCollection<Category> _categories;
        private readonly IOptionsMonitor<BookstoreDatabaseSettings> _settings;
        public CategoryService(IOptionsMonitor<BookstoreDatabaseSettings> settings)
        {
            _settings = settings;
            var client = new MongoClient(_settings.CurrentValue.ConnectionString);
            var database = client.GetDatabase(_settings.CurrentValue.DatabaseName);

            


            _categories = database.GetCollection<Category>(_settings.CurrentValue.CategoriesCollectionName);
        }

        public async Task<List<Category>> GetAsync() =>
           await _categories.Find(category => true).ToListAsync();
        public async Task<Category> GetAsync(string id) =>
           await _categories.Find<Category>(category => category.Id == id).FirstOrDefaultAsync();

        public async Task<Category> CreateAsync(Category category)
        {
            await _categories.InsertOneAsync(category);
            return category;
        }

        public async Task UpdateAsync(string id, Category categoryIn) =>
           await _categories.ReplaceOneAsync(category => category.Id == id, categoryIn);

        public async Task DeleteAsync(string id) =>
            await _categories.DeleteOneAsync(category => category.Id == id);
    }
}