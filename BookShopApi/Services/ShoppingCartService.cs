﻿using BookShopApi.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookShopApi.Services
{
    public class ShoppingCartService
    {

        private readonly IMongoCollection<ShoppingCart> _shoppingcarts;

        private readonly IOptionsMonitor<BookstoreDatabaseSettings> _settings;
        public ShoppingCartService(IOptionsMonitor<BookstoreDatabaseSettings> settings)
        {
            _settings = settings;
            var client = new MongoClient(_settings.CurrentValue.ConnectionString);
            var database = client.GetDatabase(_settings.CurrentValue.DatabaseName);


            _shoppingcarts = database.GetCollection<ShoppingCart>(_settings.CurrentValue.ShoppingCartsCollectionName);
        }
        public async Task<ShoppingCart> GetAsync(string userid) =>
            await _shoppingcarts.Find<ShoppingCart>(shoppingcart => shoppingcart.UserId == userid).FirstOrDefaultAsync();
        public async Task<ShoppingCart> CreateAsync(ShoppingCart shoppingcart)
        {
            await _shoppingcarts.InsertOneAsync(shoppingcart);
            return shoppingcart;
        }

        public async Task UpdateAsync(string id, ShoppingCart shoppingcartIn) =>
           await _shoppingcarts.ReplaceOneAsync(shoppingcart => shoppingcart.Id == id, shoppingcartIn);
    }
}
