﻿using BookShopApi.Services;
using IdentityModel.Client;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BookShopApi.Controllers
{


    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly UserService _userService;

        public AuthController(UserService userService)
        {
            _userService = userService;
        }
        


        [HttpPost]
        public async Task<ActionResult> Login(string username, string password)
        {
            var user = await _userService.GetUserAsync(username);

            if (user == null || !BCrypt.Net.BCrypt.Verify(password, user.PassWord))
                return BadRequest(new { message = "Username or password is incorrect" });


            var client = new HttpClient();

            var disco = await client.GetDiscoveryDocumentAsync("https://localhost:5001");
            if (disco.IsError)
            {
                return BadRequest(disco.Error);
            }
            
            // request token
            var tokenResponse = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                Address = disco.TokenEndpoint,
                ClientId = "client",
                ClientSecret = "secret",
                Scope = "api1",
                
                
            });

       
            if (tokenResponse.IsError)
            {
                return this.BadRequest(tokenResponse.Error);
            }

            return Ok(tokenResponse.Json);

        }
        
    }
}
