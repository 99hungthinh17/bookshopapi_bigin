﻿
using BookShopApi.Models;
using BookShopApi.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net.WebSockets;
using System.Threading.Tasks;
using BookShopApi.Controllers;
using Microsoft.AspNetCore.Authorization;

namespace BookShopApi.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private readonly BookService _bookService;
        public BooksController(BookService bookService)
        {
            _bookService = bookService;
          
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Book>>> GetAll()
        {
            var books = await _bookService.GetAsync();
            return Ok(books);
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<Book>> GetbyId(string id)
        {
            var book = await _bookService.GetAsync(id);

            if (book == null)
            {
                return NotFound();
            }
            return Ok(book);
        }
        [HttpGet("[action]")]
        public async Task<ActionResult<Book>> SearchBook(string searchString)
        {
            var books = await _bookService.SearchAsync(searchString);

            if (books == null)
            {
                return NotFound();
            }

            return Ok(books);
        }

        [HttpPost]
       
        public async Task<IActionResult> Create(Book createbook)
        {
            
             var book = await _bookService.GetAsync(createbook.Code);

            //Check book existed or not
            if (book == null)
            {
                ////Add category
                //var catergoryOfBook = await _categoryService.GetAsync(createbook.CategoryId);
                //createbook.Category = catergoryOfBook;


                await _bookService.CreateAsync(createbook);
                return Ok(createbook);
            }
            //Thông báo sách đã tồn tại
            else
            {
                return BadRequest("Book is already existed");
            }
          
        }
        [HttpPut]
       
        public async Task<IActionResult> Update(string id, Book updatedBook)
        {        

            await _bookService.UpdateAsync(id,updatedBook);
            return Ok("Update Sucessfully");
        }
        [HttpDelete] 
        public async Task<IActionResult> Delete(string id)
        {
            await _bookService.DeleteAsync(id);
            return Ok("Delete sucessfully");
        }
    }
}