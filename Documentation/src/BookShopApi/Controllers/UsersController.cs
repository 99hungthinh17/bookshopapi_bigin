﻿using BookShopApi.Models;
using BookShopApi.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using BookShopApi.Controllers;
using Microsoft.AspNetCore.Authorization;
using MongoDB.Bson.Serialization;
using BookShopApi.Validator;
using FluentValidation;
using FluentValidation.Results;

namespace BookShopApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController: ControllerBase
    {
        private readonly UserService _userService;
        private readonly ShoppingCartService _shoppingcartService;

        public UsersController(UserService userService, ShoppingCartService shoppingcartService)
        {
            _userService = userService;
            _shoppingcartService = shoppingcartService;
           
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<User>>> GetAll()
        {
            var users = await _userService.GetAsync();

           
            return Ok(users);
        }

        [HttpGet("[action]")]
        public async Task<ActionResult<Book>> Get(string id)
        {
            var user = await _userService.GetAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Create(User creatingUser)
        {
                string username = creatingUser.UserName;
                var checkedUser = await _userService.GetUserAsync(username);
                //Check username existed or not
                if (checkedUser == null)
                {

                    //Hash password
                    creatingUser.PassWord = BCrypt.Net.BCrypt.HashPassword(creatingUser.PassWord);

                    //Validate user
                    UserValidator validator = new UserValidator();
                    ValidationResult result = validator.Validate(creatingUser);

                    if (result.IsValid)
                    {
                        var createdUser = await _userService.CreateAsync(creatingUser);

                        //Create shopping cart each time create user
                        var shoppingcart = new ShoppingCart()
                        {
                            BookIds = new List<string>(),
                            UserId = createdUser.Id
                        };
                        await _shoppingcartService.CreateAsync(shoppingcart);
                        return Ok(createdUser);
                    }
                    else
                    {
                        foreach (var failure in result.Errors)
                        {
                            return BadRequest("Property " + failure.PropertyName + " failed validation. Error was: " + failure.ErrorMessage);
                        }
                    }
                }
                return BadRequest("User Name is existed");
                       
        }
        [HttpPut]
        public async Task<IActionResult> UpdatePassword(string username, string password)
        {

            var user = await _userService.GetUserAsync(username);
            user.PassWord = BCrypt.Net.BCrypt.HashPassword(password);

            await _userService.UpdateAsync(user.Id,user);
            return Ok(user);

        }
        [HttpDelete]
        public async Task<IActionResult> Delete(string id)
        {
            
            await _userService.DeleteAsync(id);
            return Ok("Delete sucessfully");
        }

    }
}
