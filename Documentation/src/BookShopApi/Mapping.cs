﻿using BookShopApi.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Bson.Serialization.Serializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookShopApi
{
    public static class Mapping
    {
        public static void MappingClasses()
        {
            BsonClassMap.RegisterClassMap<Category>(ct =>
            {
                ct.AutoMap();
                ct.MapIdProperty(c => c.Id)
                     .SetIdGenerator(StringObjectIdGenerator.Instance)
                     .SetSerializer(new StringSerializer(BsonType.ObjectId));
            });
            BsonClassMap.RegisterClassMap<Book>(b =>
            {
                b.AutoMap();
                b.MapIdProperty(x => x.Id)
                     .SetIdGenerator(StringObjectIdGenerator.Instance)
                     .SetSerializer(new StringSerializer(BsonType.ObjectId));
                b.MapMember(x => x.CategoryId).SetSerializer(new StringSerializer(BsonType.ObjectId));
            });

            BsonClassMap.RegisterClassMap<User>(u =>
            {
                u.AutoMap();
                u.MapIdProperty(x => x.Id)
                     .SetIdGenerator(StringObjectIdGenerator.Instance)
                     .SetSerializer(new StringSerializer(BsonType.ObjectId));
            });

            BsonClassMap.RegisterClassMap<ShoppingCart>(sc =>
            {
                sc.AutoMap();
                sc.MapIdProperty(x => x.Id)
                     .SetIdGenerator(StringObjectIdGenerator.Instance)
                     .SetSerializer(new StringSerializer(BsonType.ObjectId));

                sc.MapMember(x => x.BookIds)
                    .SetSerializer(
                      new EnumerableInterfaceImplementerSerializer<List<string>, string>(
                                    new StringSerializer(BsonType.ObjectId)));

                sc.GetMemberMap(c => c.Books).SetIgnoreIfNull(true);
            });
        }
    }
}
