﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace BookShopApi.Models
{
    public class Book
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string CategoryId { get; set; }
        public string Author { get; set; }
        public int Amount { get; set; } 
        public int Code { get; set; }

    }
    public class BookwithCatergory : Book
    {
        public Category[] Category { get; set; }

    }
}