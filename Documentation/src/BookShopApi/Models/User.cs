﻿using Microsoft.AspNetCore.Identity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace BookShopApi.Models
{
    public class User 
    {
        public string Id { get; set; }
        public string UserName { get; set; }

        public string FullName { get; set; }
        public string PassWord { get; set; }
        public string Email { get; set; }
        public bool isAdmin { get; set; }
    }
}
