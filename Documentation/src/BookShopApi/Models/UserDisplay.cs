﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookShopApi.Models
{
    public class UserDisplay
    {
        public string Id { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
    }
}
