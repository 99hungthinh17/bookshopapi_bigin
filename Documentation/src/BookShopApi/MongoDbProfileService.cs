﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using BookShopApi.Services;
using IdentityModel;
using IdentityServer4.Extensions;
using IdentityServer4.Models;

namespace BookShopApi
{
    public class MongoDbProfileService : IdentityServer4.Services.IProfileService
    {
        private readonly UserService _userService;

        public MongoDbProfileService(UserService userService)
        {
            _userService = userService;
        }

        public Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var subjectId = context.Subject.GetSubjectId();

            var user = _userService.GetAsync(subjectId);

            var claims = new List<Claim>
            {
                new Claim(JwtClaimTypes.Subject, user.Id.ToString()),
               
            };

            context.IssuedClaims = claims;

            return Task.FromResult(0);
        }

        public Task IsActiveAsync(IsActiveContext context)
        {
            var user = _userService.GetAsync(context.Subject.GetSubjectId());

            context.IsActive = (user != null);
            return Task.FromResult(0);
        }
    }
}
