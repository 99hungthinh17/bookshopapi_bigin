﻿
using BookShopApi.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookShopApi.Services
{
    public class BookService
    {
        private readonly IMongoCollection<Book> _books;
        private readonly IMongoCollection<Category> _categories;

        private readonly IOptionsMonitor<BookstoreDatabaseSettings> _settings;

        public BookService(IOptionsMonitor<BookstoreDatabaseSettings> settings)
        {
            _settings = settings;
            var client = new MongoClient(_settings.CurrentValue.ConnectionString);
            var database = client.GetDatabase(_settings.CurrentValue.DatabaseName);


            _books = database.GetCollection<Book>(_settings.CurrentValue.BooksCollectionName);
            _categories = database.GetCollection<Category>(_settings.CurrentValue.CategoriesCollectionName);

        }

        public async Task<List<BookwithCatergory>> GetAsync()
        {

            //var result = await _books.Aggregate()
            //           .Lookup<BookwithCatrgory>("Categories", "CategoryId", "_id", "Category")
            //           .ToListAsync();



            var result = await _books.Aggregate()
                                       .Lookup<Book, Category, BookwithCatergory>(
                                            _categories,
                                            x => x.CategoryId,
                                            x => x.Id,
                                            x => x.Category).ToListAsync();
            return result;

            //await _books.Find(book => book.Amount > 0).ToListAsync();
        }
           

        public async Task<Book> GetAsync(string id) =>
           await _books.Find<Book>(book => book.Id == id).FirstOrDefaultAsync();
        public async Task<Book> GetAsync(int code) =>
          await _books.Find<Book>(book => book.Code == code).FirstOrDefaultAsync();
        public async Task<Book> CreateAsync(Book book)
        {
            await _books.InsertOneAsync(book);
            return book;
        }

        public async Task UpdateAsync(string id, Book bookIn) =>
           await _books.ReplaceOneAsync(book => book.Id == id, bookIn);


        public async Task DeleteAsync(string id) =>
           await _books.DeleteOneAsync(book => book.Id == id);

        public async Task<List<Book>> SearchAsync(string searchstring) =>
            await _books.Find(book => book.Name.Contains(searchstring)).ToListAsync();

    }
}