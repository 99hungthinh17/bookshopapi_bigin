﻿using BookShopApi.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookShopApi.Services
{
    public class UserService
    {
        private readonly IMongoCollection<User> _users;

        private readonly IOptionsMonitor<BookstoreDatabaseSettings> _settings;

        public UserService(IOptionsMonitor<BookstoreDatabaseSettings> settings)
        {
            _settings = settings;
            var client = new MongoClient(_settings.CurrentValue.ConnectionString);
            var database = client.GetDatabase(_settings.CurrentValue.DatabaseName);
            _users = database.GetCollection<User>(_settings.CurrentValue.UsersCollectionName);

            
        }

        public async Task<IEnumerable<UserDisplay>> GetAsync()
        {
            return await _users.Find(user => true).Project(x => new UserDisplay { Id = x.Id, UserName = x.UserName, FullName = x.FullName }).ToListAsync();
        }
            
        public async Task<dynamic> GetAsync(string id) =>
           await _users.Find(user => user.Id == id).Project(x => new UserDisplay { Id = x.Id, UserName = x.UserName, FullName = x.FullName }).ToListAsync();

        public async Task<User> GetUserAsync(string username) =>
         await _users.Find<User>(user => user.UserName == username).FirstOrDefaultAsync();

        public async Task<User> CreateAsync(User user)
        {
            await _users.InsertOneAsync(user);
            return user;
        }

        public async Task UpdateAsync(string id, User userIn) =>
           await _users.ReplaceOneAsync(user => user.Id == id, userIn);

        public async Task DeleteAsync(string id) =>
            await _users.DeleteOneAsync(user => user.Id == id);

       
    }
}
