﻿using BookShopApi.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookShopApi.Validator
{
    public class UserValidator: AbstractValidator<User>
    {
        public UserValidator()
        {
            RuleFor(user => user.FullName).NotNull();
            RuleFor(user => user.UserName).NotNull();
            RuleFor(user => user.PassWord).NotNull();
            RuleFor(user => user.Email).EmailAddress();
        }
    }
}
